/*
 *****************************************************************************
 * MainPageResultListFragment.kt
 *****************************************************************************
 * Copyright © 2016-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
package org.videolan.vlcbenchmark

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.util.Pair
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
import org.videolan.vlcbenchmark.results.ResultRepository
import org.videolan.vlcbenchmark.tools.FormatStr.format2Dec
import org.videolan.vlcbenchmark.tools.FormatStr.fromDatePrettyPrint
import org.videolan.vlcbenchmark.tools.FormatStr.strDateToDate
import org.videolan.vlcbenchmark.tools.FormatStr.toDatePrettyPrint
import org.videolan.vlcbenchmark.tools.StorageManager
import org.videolan.vlcbenchmark.tools.StorageManager.delete
import org.videolan.vlcbenchmark.tools.StorageManager.getInternalDirStr
import org.videolan.vlcbenchmark.tools.Util.runInBackground
import org.videolan.vlcbenchmark.tools.Util.runInUiThread
import java.io.File
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class MainPageResultListFragment : Fragment() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var data: ArrayList<Pair<String, String>>
    private lateinit var noResultsLayout: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_main_page_result_list_fragment, container, false)
        if (view == null) {
            Log.e(TAG, "onCreateView: view is null")
            return null
        }
        noResultsLayout = view.findViewById<LinearLayout>(R.id.no_results)
        noResultsLayout.isFocusable = false
        recyclerView = view.findViewById(R.id.test_list_view)
        recyclerView.setHasFixedSize(true)
        recyclerView.isFocusable = true
        val layoutManager = LinearLayoutManager(context)
        recyclerView.layoutManager = layoutManager
        recyclerView.addItemDecoration(DividerItemDecoration(requireContext(), LinearLayoutManager.VERTICAL))
        data = ArrayList()
        val adapter = TestListAdapter(data)
        adapter.registerAdapterDataObserver(object : AdapterDataObserver() {
            override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) {
                setVisibility()
                super.onItemRangeRemoved(positionStart, itemCount)
            }
        })
        recyclerView.adapter = adapter
        val filenames = ResultRepository().getAllResultListIds()
        if (filenames.isEmpty()) {
            noResultsLayout.visibility = View.VISIBLE
        } else {
            val tmpData = orderBenchmarks(filenames)
            runInBackground(Runnable {
                for (filename in tmpData) {
                    val results = ResultRepository().getResultList("$filename.txt")
                    val title = toDatePrettyPrint(filename)
                    if (context == null) {
                        Log.w(TAG, "No context for background json loading," +
                                " fragment must have been replaced")
                        return@Runnable
                    }
                    var score = 0
                    for (test in results) {
                        score += test.score.toInt()
                    }
                    val text = String.format(resources.getString(R.string.result_score_value),
                            format2Dec(score.toDouble()))
                    runInUiThread(Runnable { addResult(title, text) })
                }
            })
        }
        return view
    }

    /**
     * To be called when adding or removing an item from the recycler view.
     * if empty displays the empty view
     * else display the recycler view  */
    private fun setVisibility() {
        if (data.isEmpty()) {
            noResultsLayout.visibility = View.VISIBLE
        } else {
            noResultsLayout.visibility = View.GONE
        }
    }

    private fun addResult(name: String, result: String) {
        data.add(Pair(name, result))
        recyclerView.adapter?.notifyItemInserted(data.size)
        setVisibility()
    }

    val isEmpty: Boolean
        get() = data.isEmpty()
    val isScrollingPositionTop: Boolean
        get() = if (recyclerView.findViewHolderForLayoutPosition(0) != null) {
            recyclerView.findViewHolderForLayoutPosition(0)?.itemView?.hasFocus() ?: false
        } else false

    fun scrollingBackToTop() {
        recyclerView.findViewHolderForLayoutPosition(0)?.itemView?.requestFocus()
    }

    override fun onResume() {
        super.onResume()
        if (scrollPosition != -1) {
            recyclerView.layoutManager?.scrollToPosition(scrollPosition)
        }
    }

    override fun onPause() {
        super.onPause()
        (recyclerView.layoutManager as LinearLayoutManager?)?.let {
            scrollPosition = it.findFirstCompletelyVisibleItemPosition()
        }
    }

    inner class TestListAdapter internal constructor(var data: ArrayList<Pair<String, String>>) : RecyclerView.Adapter<TestListAdapter.ViewHolder>() {
        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            var title: TextView = view.findViewById(R.id.result_name)
            var result: TextView = view.findViewById(R.id.score)

            init {
                view.setOnClickListener {
                    val text = view.findViewById<TextView>(R.id.result_name)
                    val intent = Intent(activity, ResultPage::class.java)
                    intent.putExtra(Constants.EXTRA_RESULT_PAGE_NAME, fromDatePrettyPrint(text.text.toString()))
                    startActivityForResult(intent, Constants.RequestCodes.RESULTS)
                }
                view.setOnLongClickListener {
                    AlertDialog.Builder(requireActivity())
                            .setTitle(R.string.dialog_title_warning)
                            .setMessage(R.string.dialog_text_confirm_bench_delete)
                            .setNeutralButton(R.string.dialog_btn_cancel, null)
                            .setNegativeButton(R.string.dialog_btn_continue) { _: DialogInterface?, _: Int ->
                                val sample = File(getInternalDirStr(StorageManager.jsonFolder)
                                        + fromDatePrettyPrint(title.text.toString())
                                        + ".txt")
                                delete(sample)
                                data.removeAt(adapterPosition)
                                recyclerView.removeViewAt(adapterPosition)
                                notifyItemRemoved(adapterPosition)
                            }
                            .show()
                    true
                }
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.test_list_rows, parent, false)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.title.text = data[position].first
            holder.result.text = data[position].second
        }

        override fun getItemCount(): Int {
            return data.size
        }
    }

    private fun orderBenchmarks(str_dates: ArrayList<String>): ArrayList<String> {
        str_dates.sortWith(kotlin.Comparator { o1, o2 ->
            val d1 = strDateToDate(o1)
            val d2 = strDateToDate(o2)
            d1!!.compareTo(d2)
        })
        str_dates.reverse()
        return str_dates
    }

    companion object {
        private val TAG = MainPageResultListFragment::class.java.name
        private var scrollPosition = -1
    }
}