package org.videolan.vlcbenchmark

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class ProgressBottomSheet: BottomSheetDialogFragment() {
    companion object {
        @Suppress("UNUSED")
        private val TAG = this::class.java.name
    }

    var titleId: Int = 0
    lateinit var title: TextView
    lateinit var text: TextView
    lateinit var currentSample: TextView
    private lateinit var progressBar: ProgressBar
    lateinit var cancel: () -> Unit

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.progress_bottom_sheet, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        title = view.findViewById(R.id.download_title)
        text = view.findViewById(R.id.old_download_text)
        currentSample = view.findViewById(R.id.benchmark_sample_name)
        progressBar = view.findViewById(R.id.old_download_progress_bar)

        view.keepScreenOn = true

        if (titleId == 0) {
            this.dismiss()
        } else {
            title.setText(titleId)
        }
        progressBar.progress = 0
        progressBar.max = 100
        text.setText(R.string.default_percent_value)

        val button: Button = view.findViewById(R.id.old_download_btn_cancel)
        button.setOnClickListener {
            cancel()
            this.dismiss()
        }
    }

    fun setTitle(titleId: Int) {
        this.titleId = titleId
    }

    fun setCancelCallback(cancel: () -> Unit) {
        this.cancel = cancel
    }


    fun updateProgress(value: Double, text: String) {
        progressBar.progress = value.toInt()
        this.text.text = text
    }
}