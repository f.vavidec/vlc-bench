package org.videolan.vlcbenchmark

import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.videolan.vlcbenchmark.benchmark.StopNotificationBroadcastReceiver
import org.videolan.vlcbenchmark.tools.FormatStr
import org.videolan.vlcbenchmark.tools.StorageManager
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class SampleTransferService : LifecycleService() {
    private val notificationChannelId = "VLCBENCHMARK SAMPLE TRANSFER CHANNEL"
    private val notificationIntId = 2

    private var totalCopySize: Long = 0L

    private var cancelled = false
    private lateinit var notificationManager: NotificationManager
    private lateinit var notificationBuilder: Notification.Builder
    private var errorStringId = 0


    private var br = SampleTransferServiceBroadcastReceiver()
    private var binder: Binder = SampleTransferServiceBinder()

    override fun onBind(intent: Intent): IBinder {
        super.onBind(intent)
        return binder
    }

    override fun onCreate() {
        super.onCreate()
        val filter = IntentFilter(Constants.ACTION_CANCEL_SAMPLE_TRANSFERT)
        filter.addAction(Constants.ACTION_START_SAMPLE_TRANSFER)
        registerReceiver(br, filter)
        startForeground(notificationIntId, createNotification())
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        intent?.let {
            when (intent.action) {
                Constants.ACTION_START_SAMPLE_TRANSFER -> {
                    lifecycleScope.launch(Dispatchers.IO) {
                        val oldDir = intent.getStringExtra(Constants.EXTRA_SAMPLE_TRANSFER_OLD_DIR) ?: ""
                        val newDir = intent.getStringExtra(Constants.EXTRA_SAMPLE_TRANSFER_NEW_DIR) ?: ""
                        if (oldDir != "" && newDir != "") {
                            totalCopySize = StorageManager.getDirectoryMemoryUsage(oldDir)
                            val transferedSize = transferMountpoints(oldDir, newDir, 0L)
                            if (transferedSize != -1L) {
                                val brIntent = Intent(Constants.ACTION_FINISHED_SAMPLE_TRANSFER)
                                brIntent.putExtra(Constants.EXTRA_SAMPLE_TRANSFER_NEW_DIR, newDir)
                                sendBroadcast(brIntent)
                            } else {
                                val brIntent = Intent(Constants.ACTION_SERVICE_ERROR)
                                if (errorStringId != 0)
                                    brIntent.putExtra(Constants.EXTRA_SERVICE_ERROR, errorStringId)
                                sendBroadcast(brIntent)
                            }
                        } else {
                            val brIntent = Intent(Constants.ACTION_SERVICE_ERROR)
                            brIntent.putExtra(Constants.EXTRA_SERVICE_ERROR, R.string.error_service_fail_start)
                            sendBroadcast(brIntent)
                            stopSelf()
                        }
                    }
                }
                Constants.ACTION_STOP_TRANSFERT_SERVICE -> {
                    stopSelf()
                }
                else -> {}
            }
        }
        return super.onStartCommand(intent, flags, startId)
    }

    private fun createNotification(): Notification {
        // depending on the Android API that we're dealing with we will have
        // to use a specific method to create the notification
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            val channel = NotificationChannel(
                notificationChannelId,
                "VLCBenchmark notifications channel",
                NotificationManager.IMPORTANCE_LOW
            ).let {
                it.description = getString(R.string.notif_transfert_channel_description)
                it
            }
            notificationManager.createNotificationChannel(channel)
        }

        val pendingIntent: PendingIntent = Intent(this, VLCWorkerModel::class.java).let { notificationIntent ->
            PendingIntent.getActivity(this, 0, notificationIntent, 0)
        }
        val stopIntent = Intent(this, StopNotificationBroadcastReceiver::class.java).apply {
            action = Constants.ACTION_STOP_TRANSFERT_SERVICE
            putExtra(Constants.EXTRA_NOTIFICATION_ID, 0)
        }
        val stopPendingIntent: PendingIntent = PendingIntent.getBroadcast(this, 0, stopIntent, 0)

        notificationBuilder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) Notification.Builder(
            this,
            notificationChannelId
        ) else Notification.Builder(this)

        notificationBuilder
            .setContentTitle(getString(R.string.notif_transfert_content_title))
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.drawable.ic_icon_benchmark_white)
            .setTicker(getString(R.string.notif_transfert_content_title))
            .addAction(android.R.drawable.ic_menu_close_clear_cancel, getString(R.string.notification_btn_close), stopPendingIntent)
            .setPriority(Notification.PRIORITY_LOW) // for under android 26 compatibility
            .setProgress(100, 0, false)
        return notificationBuilder.build()
    }

    private fun transferMountpoints(oldDirectory: String, newDirectory: String, _transferedSize: Long): Long {
        val oldDir = File(oldDirectory)
        val files = oldDir.listFiles()
        var transferedSize = _transferedSize
        StorageManager.checkFolderLocation(newDirectory)
        if (files != null) {
            for (f in files) {
                if (cancelled) {
                    Log.w(TAG, "transferMountpoints: Task was cancelled")
                    return -1
                }
                if (f.isDirectory) {
                    transferedSize = transferMountpoints("$oldDirectory/${f.name}",
                        "$newDirectory/${f.name}", transferedSize)
                } else {
                    val newFile = File("$newDirectory/${f.name}")
                    try {
                        val size = 2 * 1024
                        try {
                            val fin = FileInputStream(f)
                            val fout = FileOutputStream(newFile)
                            val buffer = ByteArray(size)
                            var read = fin.read(buffer, 0, size)
                            var fromTime = System.nanoTime()
                            var passedTime: Long
                            var passedSize: Long = 0
                            var fileDownloadSize: Long = 0
                            while (read != -1) {
                                if (cancelled) {
                                    Log.w(TAG, "transferMountpoints: Task was cancelled")
                                    return -1
                                }
                                passedTime = System.nanoTime()
                                fout.write(buffer, 0, read)
                                passedSize += read.toLong()
                                if (passedTime - fromTime >= 1_000_000_000L) {
                                    fileDownloadSize += passedSize
                                    publishProgress(totalCopySize, transferedSize + fileDownloadSize, passedSize)
                                    fromTime = System.nanoTime()
                                    passedSize = 0
                                }
                                read = fin.read(buffer, 0, size)
                            }
                            fin.close()
                            fout.close()
                            val oldChecksum = StorageManager.getFileChecksum(f)
                            val newChecksum = StorageManager.getFileChecksum(newFile)
                            if (oldChecksum != newChecksum) {
                                Log.e(TAG, "copyFile: Copy has failed: file checkums differ")
                                return -1
                            }
                        } catch (e: Exception) {
                            Log.e(TAG, "transferMountpoints: $e")
                            return -1
                        }
                        transferedSize += f.length()
                        publishProgress(totalCopySize, transferedSize, 0L)
                    } catch (e: Exception) {
                        Log.e(TAG, "transferMountpoints: $e")
                        return -1
                    }
                }
            }
        } else {
            Log.i(TAG, "transferMountpoints: There are no files to transfert")
        }
        return transferedSize
    }

    private fun publishProgress(totalSize: Long, downloadSize: Long, downloadSpeed: Long) {
        val percent = downloadSize.toDouble() / totalSize * 100.0
        val progressString = String.format(
            getString(R.string.dialog_text_download_progress),
            FormatStr.format2Dec(percent),
            FormatStr.byteRateToString(applicationContext, downloadSpeed),
            FormatStr.byteSizeToString(applicationContext, downloadSize),
            FormatStr.byteSizeToString(applicationContext, totalSize)
        )

        val broadcastIntent = Intent(Constants.ACTION_UPDATE_PROGRESS)
        broadcastIntent.putExtra(Constants.EXTRA_DOWNLOAD_PERCENT, percent)
        broadcastIntent.putExtra(Constants.EXTRA_DOWNLOAD_STRING, progressString)
        sendBroadcast(broadcastIntent)

        notificationBuilder.setContentText(progressString)
        notificationBuilder.setProgress(100, percent.toInt(), false)
        notificationManager.notify(notificationIntId, notificationBuilder.build())
    }

    inner class SampleTransferServiceBroadcastReceiver: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.let {
                when (it.action) {
                    Constants.ACTION_CANCEL_SAMPLE_TRANSFERT -> {
                        cancelled = true
                    }
                    else -> {}
                }
            }
        }
    }

    override fun onDestroy() {
        unregisterReceiver(br)
        super.onDestroy()
    }

    inner class SampleTransferServiceBinder: Binder() {
        fun getService() : SampleTransferService = this@SampleTransferService
    }

    companion object {
        @Suppress("UNUSED")
        private val TAG = this::class.java.name
    }
}