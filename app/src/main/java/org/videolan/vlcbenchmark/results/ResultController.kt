/*
 *****************************************************************************
 * ResultController.kt
 *****************************************************************************
 * Copyright © 2020-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package org.videolan.vlcbenchmark.results

import android.content.Context
import android.os.Parcelable
import android.util.Log
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.results.types.ResultPlayback
import org.videolan.vlcbenchmark.results.types.ResultQuality
import org.videolan.vlcbenchmark.results.types.ResultSpeed

@Parcelize
class ResultController(
        var currentLoopIndex: Int = 0,
        var maxLoop: Int = 0
) : Parcelable {

    fun reset(context: Context, numberOfLoops: Int) {
        ResultRepository().discardCurrentResultList(context)
        maxLoop = numberOfLoops
        val resultList: Array<ArrayList<ResultModel>> = if (maxLoop == 1) {
            arrayOf(ArrayList())
        } else {
            arrayOf(ArrayList(), ArrayList(), ArrayList())
        }
        ResultRepository().setCurrentResultList(context, resultList)
    }

    fun isLastLoop() : Boolean {
        if (currentLoopIndex == maxLoop - 1)
            return true
        return false
    }

    fun nextLoop() {
        currentLoopIndex += 1
    }

    fun setResultList(context: Context, resultArray: Array<ArrayList<ResultModel>>, numberOfLoops: Int) : Int {
        if (numberOfLoops != resultArray.size) {
            ResultRepository().discardCurrentResultList(context)
            return -1
        } else if (resultArray.isEmpty()) {
            return -1
        }
        maxLoop = numberOfLoops
        var loopIndex = 0
        // finding the benchmark loop
        if (numberOfLoops == 3) {
            while (loopIndex < numberOfLoops) {
                if (resultArray[loopIndex].size == 0) {
                    if (loopIndex == 0) {
                        return -1
                    } else {
                        break;
                    }
                }
                loopIndex += 1
            }
            currentLoopIndex = if (loopIndex == 0) 0 else loopIndex - 1
        } else if (numberOfLoops == 1 && resultArray[loopIndex].size == 0) {
            return -1
        }
        return resultArray[currentLoopIndex].size
    }

    fun addResults(context: Context, result: ResultModel) {
        ResultRepository().addToCurrentResultList(context, result, currentLoopIndex)
        // Clearing stacktrace so that it is stored in json, but not in app memory
        result.clearStacktrace()
    }

    companion object {
        @Suppress("UNUSED")
        private val TAG = this::class.java.name

        fun mergeResults(results: Array<ArrayList<ResultModel>>): ArrayList<ResultModel>? {
            val mergedResults = ArrayList<ResultModel>()
            for (i in 0 until results[0].size) {
                val res = results[0][i]
                for (j in 1 until results.size) {
                    val toComp = results[j][i]
                    if (res.name != toComp.name || res.type != toComp.type || res.hardware != toComp.hardware) {
                        Log.e(TAG, "mergeResults: Difference in ResultModels")
                        return null
                    }
                    res.score += toComp.score
                    when (res.type) {
                        Constants.ResultType.QUALITY -> {
                            for (inc in 0 until (res as ResultQuality).screenshotScores.size) {
                                res.screenshotScores[inc] += (toComp as ResultQuality).screenshotScores[inc]
                            }
                        }
                        Constants.ResultType.PLAYBACK -> {
                            (res as ResultPlayback).warnings += (toComp as ResultPlayback).warnings
                            (res as ResultPlayback).framesDropped += toComp.framesDropped
                        }
                        Constants.ResultType.SPEED -> {
                            (res as ResultSpeed).speed += (toComp as ResultSpeed).speed
                        }
                        else -> {
                            Log.e(TAG, "mergeResults: Unknown result type")
                            return null
                        }
                    }
                }
                when (res.type) {
                    Constants.ResultType.QUALITY -> {
                        for (inc in 0 until (res as ResultQuality).screenshotScores.size) {
                            res.screenshotScores[inc] /= results.size
                        }
                    }
                    Constants.ResultType.PLAYBACK -> {
                        (res as ResultPlayback).warnings /= results.size
                        (res as ResultPlayback).framesDropped /= results.size
                    }
                    Constants.ResultType.SPEED -> {
                        (res as ResultSpeed).speed /= results.size
                    }
                    else -> {
                        Log.e(TAG, "mergeResults: Unknown result type")
                        return null
                    }
                }
                res.score /= results.size
                mergedResults.add(res)
            }
            return mergedResults
        }
    }
}