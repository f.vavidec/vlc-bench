/*
 *****************************************************************************
 * ApiCalls.kt
 *****************************************************************************
 * Copyright © 2020-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package org.videolan.vlcbenchmark.api

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.util.Log
import android.view.LayoutInflater
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import org.json.JSONObject
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.R
import org.videolan.vlcbenchmark.ResultPage
import org.videolan.vlcbenchmark.results.ResultModel
import org.videolan.vlcbenchmark.results.types.ResultQuality
import org.videolan.vlcbenchmark.tools.StorageManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.util.*

class ApiCalls {

    companion object {

        @Suppress("UNUSED")
        private val TAG = this::class.java.name

        private fun displayProgressDialog(context: Context, call: Call<out Any>): Dialog {
            val cancelCallback = DialogInterface.OnClickListener { dialog, _ ->
                call.cancel()
                dialog.dismiss()
            }
            val view = LayoutInflater.from(context).inflate(R.layout.layout_upoad_progress_dialog, null)
            view.keepScreenOn = true
            return AlertDialog.Builder(context)
                    .setView(view)
                    .setNegativeButton(R.string.dialog_btn_cancel, cancelCallback)
                    .show()
        }

        private fun getUploadCallback(context: Context, progressDialog: Dialog?, successListener: (success: Boolean) -> Unit): Callback<Void> {
            return object : Callback<Void> {
                override fun onResponse(call: Call<Void>, response: Response<Void>) {
                    progressDialog?.dismiss()
                    if (response.code() == 200) {
                        successListener(true)
                        StorageManager.deleteScreenshots()
                    } else {
                        Log.e(TAG, response.toString())
                        successListener(false)
                    }
                }

                override fun onFailure(call: Call<Void>, t: Throwable) {
                    Log.e(TAG, t.toString())
                    progressDialog?.dismiss()
                    successListener(false)
                }
            }
        }

        @JvmStatic
        fun uploadBenchmark(context: Context, res: JSONObject, successListener: (success: Boolean) -> Unit) {
            val retrofit = RetrofitInstance.retrofit
            if (retrofit != null) {
                val apiInterface = retrofit.create(ApiInterface::class.java)
                val call = apiInterface.uploadBenchmark(
                        RequestBody.create(
                                MediaType.parse("application/json; charset=utf-8"),
                                res.toString()
                        )
                )
                var progressDialog: Dialog? = null
                if (context is ResultPage)
                    progressDialog = displayProgressDialog(context, call)
                val callback = getUploadCallback(context, progressDialog, successListener)
                call.enqueue(callback)
            } else {
                Log.e(TAG, "uploadBenchmark: Retrofit is null")
            }
        }

        @JvmStatic
        fun uploadBenchmarkWithScreenshots(context: Context, jsonObject: JSONObject, results: ArrayList<ResultModel>, successListener: (success: Boolean) -> Unit) {
            val retrofit = RetrofitInstance.retrofit
            if (retrofit != null) {
                val apiInterface = retrofit.create(ApiInterface::class.java)
                val builder = MultipartBody.Builder()
                builder.setType(MultipartBody.FORM)
                builder.addFormDataPart("data", jsonObject.toString())

                val screenshotFolder = StorageManager.getInternalDirStr(StorageManager.screenshotFolder)
                for (test in results) {
                    if (test.type == Constants.ResultType.QUALITY) {
                        for (screen in (test as ResultQuality).screenshotNames) {
                            if (screen != "") {
                                val file = File(screenshotFolder, screen)
                                builder.addFormDataPart(
                                    screen,
                                    screen,
                                    RequestBody.create(
                                        MediaType.parse("multipart/form-data"),
                                        file
                                    )
                                )
                            }
                        }
                    }
                }

                val requestBody = builder.build()
                val call = apiInterface.uploadBenchmarkWithScreenshots(requestBody)
                val progressDialog = displayProgressDialog(context, call)
                val callback = getUploadCallback(context, progressDialog, successListener)
                call.enqueue(callback)
            } else {
                Log.e(TAG, "uploadBenchmarkWithScreenshots: Retrofit is null")
            }
        }
    }
}