/*
 *****************************************************************************
 * FormatStr.kt
 *****************************************************************************
 * Copyright © 2017-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package org.videolan.vlcbenchmark.tools

import android.content.Context
import android.util.Log
import org.videolan.vlcbenchmark.R
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object FormatStr {

    private val TAG = FormatStr::class.java.name

    private val decimalFormat_2 = DecimalFormat("#.##")

    val dateStr: String
        get() {
            val format = SimpleDateFormat("yyMMddHHmmssZ", Locale.getDefault())
            return format.format(System.currentTimeMillis())
        }

    fun format2Dec(number: Double): String {
        return decimalFormat_2.format(number)
    }

    fun strDateToDate(str_date: String): Date? {
        val format = SimpleDateFormat("yyMMddHHmmssZ", Locale.getDefault())
        var date: Date? = null
        try {
            date = format.parse(str_date)
        } catch (e: ParseException) {
            Log.e(TAG, "strDateToDate: Failed to parse date: $str_date\n$e")
        }

        return date
    }

    /**
     * Converts date string used for filenames
     * to pretty readable date string ("dd MMM yyyy HH:mm:ss")
     * @param date_str filename date string
     * @return readable date string
     */
    fun toDatePrettyPrint(date_str: String): String {
        var date_str = date_str
        var format = SimpleDateFormat("yyMMddHHmmssZ", Locale.getDefault())
        var date: Date? = null
        try {
            date = format.parse(date_str)
        } catch (e: ParseException) {
            Log.e(TAG, "Failed to parse date: $date_str\n$e")
        }

        format = SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.getDefault())
        date_str = format.format(date)
        return date_str
    }

    /**
     * Converts the readable date string ("dd MMM yyyy HH:mm:ss")
     * to the date string for filenames
     * @param date_str pretty readable date
     * @return underscore separated date string
     */
    fun fromDatePrettyPrint(date_str: String): String {
        var format = SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.getDefault())
        var date: Date? = null
        try {
            date = format.parse(date_str)
        } catch (e: ParseException) {
            Log.e(TAG, "Failed to parse date: $e")
        }

        format = SimpleDateFormat("yyMMddHHmmssZ", Locale.getDefault())
        return format.format(date)
    }

    fun byteRateToString(context: Context, bitRate: Long): String {
        if (bitRate <= 0)
            return "0 " + context.getString(R.string.size_unit_per_second_byte)

        val byteRate = bitRate / 8.0
        return if (byteRate > 1_000_000.0) {
            format2Dec(byteRate / 1_000_000) + " " + context.getString(R.string.size_unit_per_second_mega)
        } else {
            format2Dec(byteRate / 1_000.0) + " " + context.getString(R.string.size_unit_per_second_kilo)
        }
    }

    fun byteSizeToString(context: Context, size: Long): String {
        val unit: String
        val prettySize: Double
        if (size / 1_000_000_000 > 0) {
            unit = context.getString(R.string.size_unit_giga)
            prettySize = size / 1_000_000_000.0
        } else {
            unit = context.getString(R.string.size_unit_mega)
            prettySize = size / 1_000_000.0
        }
        return format2Dec(prettySize) + " " + unit
    }
}
