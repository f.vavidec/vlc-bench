/*
 *****************************************************************************
 * SystemPropertiesProxy.java
 *****************************************************************************
 * Copyright © 2016-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
package org.videolan.vlcbenchmark.tools

import android.app.Activity
import android.content.Context
import android.os.Build
import android.os.StatFs
import android.util.DisplayMetrics
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.videolan.vlcbenchmark.Constants
import org.videolan.vlcbenchmark.tools.FormatStr.format2Dec
import org.videolan.vlcbenchmark.tools.StorageManager.getInternalDirStr
import java.io.*
import java.net.URL
import java.nio.charset.Charset

/**
 * SystemPropertiesProxy gets
 */
object SystemPropertiesProxy {
    /* String[] to store the unit indicator (ex: Gigabyte -> G)
    * in use for getReadableValue(...)*/
    private val ext = arrayOf("K", "M", "G")
    private val TAG = this::class.java.name

    /**
     * Get the value for the given key.
     *
     * @return an empty string if the key isn't found
     * @throws IllegalArgumentException if the key exceeds 32 characters
     */
    @Throws(IllegalArgumentException::class)
    operator fun get(key: String): String {
        return try {
            val systemProperties = Class.forName("android.os.SystemProperties")

            /* Parameters Types */
            val paramTypes = arrayOf<Class<*>>(String::class.java)
            val get = systemProperties.getMethod("get", *paramTypes)

            /* Parameters */
            val params = arrayOf<Any>(key)
            get.invoke(systemProperties, *params) as String
        } catch (iAE: IllegalArgumentException) {
            throw iAE
        } catch (e: Exception) {
            ""
        }
    }

    /**
     * Reads spec from system files
     * @param filename file to open
     * @param info attribute to recover
     * @return attribute string after ': ' in file line
     */
    private fun readSysFile(filename: String, info: String): String? {
        try {
            val file = FileReader(filename)
            val reader = BufferedReader(file)
            var line = reader.readLine()
            while (line != null) {
                if (line.regionMatches(0, info, 0, info.length)) {
                    val splits = line.split(": ").toTypedArray()
                    if (splits.size > 1) {
                        return splits[1].trim { it <= ' ' }
                    }
                }
                line = reader.readLine()
            }
        } catch (e: FileNotFoundException) {
            return null
        } catch (e: IOException) {
            return null
        }
        return null
    }

    /**
     * Converts a number to a human readable string form with unit
     * @param value int value to convert to readable form
     * @return readable string
     */
    private fun getReadableValue(value: Int): String {
        var count = 0
        var number = value.toDouble()
        while (number / 1000.0 > 1.0) {
            number /= 1000.0
            count += 1
        }
        return format2Dec(number) + " " + ext[count]
    }

    /**
     * Returns the cpu model
     * @return string with cpu model
     */
    @JvmStatic
    val cpuModel: String?
        get() = readSysFile("/proc/cpuinfo", "Hardware")

    /**
     * Reads and returns total Ram
     * @return string with total ram and unit
     */
    @JvmStatic
    val ramTotal: String?
        get() {
            var info = readSysFile("proc/meminfo", "MemTotal")
            if (info != null) {
                info = info.replaceFirst(" kB".toRegex(), "")
                info = getReadableValue(info.toInt()) + "B"
            }
            return info
        }

    /**
     * Reads and returns cpu minimum frequency
     * @return string with cpu minimum frequency and unit
     */
    @JvmStatic
    val cpuMinFreq: String?
        get() = try {
            val file = FileReader("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq")
            val reader = BufferedReader(file)
            val min_freq = reader.readLine().toInt()
            getReadableValue(min_freq) + "Hz"
        } catch (e: IOException) {
            null
        }

    /**
     * Reads and returns cpu maximum frequency
     * @return string with cpu maximum frequency and unit
     */
    @JvmStatic
    val cpuMaxFreq: String?
        get() = try {
            val file = FileReader("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq")
            val reader = BufferedReader(file)
            val max_freq = reader.readLine().toInt()
            getReadableValue(max_freq) + "Hz"
        } catch (e: IOException) {
            null
        }

    /**
     * Returns the number of available cpu cores
     * @return string with the number of available cpu cores
     */
    val cpuCoreNumber: String
        get() = Integer.toString(Runtime.getRuntime().availableProcessors())

    @JvmStatic
    fun getResolution(context: Activity): String {
        val metrics = DisplayMetrics()
        context.windowManager.defaultDisplay.getRealMetrics(metrics)
        return metrics.widthPixels.toString() + "x" + metrics.heightPixels
    }

    @JvmStatic
    val freeSpace: Long
        get() {
            val mediaDir = getInternalDirStr(StorageManager.mediaFolder)
                    ?: return -1L
            val file = File(mediaDir)
            val stats = StatFs(file.absolutePath)
            return stats.availableBytes
        }

    /**
     * Build.MODEL returns the model for the current device but as a code not user readable
     * This will open an equivalence table from Model to Marketing Model
     * ex: SM-F700F -> Galaxy Z Flip
     */
    private fun getProperDeviceNameFromInternet(): String {
        try {
            val url = URL("https://storage.googleapis.com/play_public/supported_devices.csv")
            val connection = url.openConnection()
            val input = connection.getInputStream()
            val br = BufferedReader(InputStreamReader(input, Charset.forName("UTF-16")))
            var line = br.readLine()
            while (line != null) {
                val split = line.split(",")
                if (split[3] == Build.MODEL)
                    return split[1]
                line = br.readLine()
            }
            br.close()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return ""
    }

    /**
     * Returns the proper device name, not just a model code
     * ex: SM-F700F -> Galaxy Z Flip
     * This is done by checking sharedPrefs for a cached value, or downloading a reference table
     */
    suspend fun getProperDeviceName(context: Context) : String{
        return withContext(Dispatchers.IO) {
            val sharedPref = context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
            if (sharedPref.contains(Constants.SHARED_PREFERENCE_MODEL)) {
                return@withContext sharedPref.getString(Constants.SHARED_PREFERENCE_MODEL, Build.MODEL) ?: ""
            }
            val name = getProperDeviceNameFromInternet()
            if (name != "") {
                val editor = sharedPref.edit()
                editor.putString(Constants.SHARED_PREFERENCE_MODEL, name)
                editor.apply()
                return@withContext name
            }
            return@withContext Build.MODEL
        }
    }


}