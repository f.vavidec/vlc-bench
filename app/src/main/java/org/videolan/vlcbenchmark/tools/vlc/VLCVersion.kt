package org.videolan.vlcbenchmark.tools.vlc


class VLCVersion(private val versionName: String): Comparable<VLCVersion> {
    val version: String
    val type: VersionType
    private val stagingVersion: Int

    enum class VersionType {
        UNKNOWN, DEV, ALPHA, BETA, RC, RELEASE
    }

    init {
        if (versionName == "") {
            type = VersionType.UNKNOWN
            version = ""
            stagingVersion = 0
        } else {
            val array = versionName.split(" ")

            version = if (array.isNotEmpty())
                array[0]
            else
                ""

            type = if (array.size >= 2) {
                when (array[1].toLowerCase()) {
                    "rc" -> VersionType.RC
                    "beta" -> VersionType.BETA
                    "alpha" -> VersionType.ALPHA
                    "dev" -> VersionType.DEV
                    else -> VersionType.UNKNOWN
                }
            } else {
                VersionType.RELEASE
            }

            stagingVersion = if (array.size >= 3) {
                array[2].toInt()
            } else {
                0
            }
        }
    }

    override fun compareTo(other: VLCVersion): Int {
        if (versionName == other.versionName)
            return 0
        val versionValue = compareVersion(other)
        if (versionValue != 0)
            return versionValue

        if (type < other.type)
            return -1
        else if (type > other.type)
            return 1

        return stagingVersion.compareTo(other.stagingVersion)
    }

    private fun compareVersion(other: VLCVersion): Int {
        val o = other.version.split(".")
        val v = version.split(".")
        for (i in o.indices) {
            val cmp = v[i].toInt().compareTo(o[i].toInt())
            if (cmp != 0)
                return cmp
        }
        return 0
    }

    override fun toString(): String {
        return versionName
    }
}