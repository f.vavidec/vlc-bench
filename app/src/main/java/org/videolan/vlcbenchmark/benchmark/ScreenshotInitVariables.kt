package org.videolan.vlcbenchmark.benchmark

import android.media.projection.MediaProjection

object ScreenshotInitVariables {
    var height: Int = 0
    var width: Int = 0
    var mediaProjection: MediaProjection? = null
    var dpi: Int = 0
}