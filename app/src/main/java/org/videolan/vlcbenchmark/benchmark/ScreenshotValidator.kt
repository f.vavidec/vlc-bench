/*
 *****************************************************************************
 * ScreenshotValidator.kt
 *****************************************************************************
 * Copyright © 2016-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/
package org.videolan.vlcbenchmark.benchmark

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.util.Log
import androidx.core.util.Pair
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import kotlin.math.floor

/**
 * ScreenshotValidator compares the screenshots taken during the benchmark
 * with the reference for the sample.
 * Each screenshot is divided into wbNumber blocks in width and hbNumber blocks in height
 * On each blocks the average color is computed and compared to the reference to
 * get a percentage of difference
 * Depending on that error margin percentage, the screenshot is validated or not
 */
object ScreenshotValidator {
    private val TAG = ScreenshotValidator::class.java.name

    /* Error margin on color difference cumulative percentage */ /* here set fairly high as to handle frame difference due to vlc imprecision */
    private const val MAX_SCREENSHOT_COLOR_DIFFERENCE_PERCENT = 55.0

    /* number of blocks in image width */
    private const val wbNumber = 6

    /* number of blocks in image height */
    private const val hbNumber = 5

    /**
     * Takes an int with YCbCr values encoded int and decodes it
     * @param ycbcrInt int encoded with YCbCr values
     * @return array with YCbCr values decoded
     */
    private fun getYCbCr(ycbcrInt: Int): IntArray {
        val ycbcrArray = IntArray(3)
        ycbcrArray[0] = ycbcrInt shr 16 and 0xFF
        ycbcrArray[1] = ycbcrInt shr 8 and 0xFF
        ycbcrArray[2] = ycbcrInt and 0xFF
        return ycbcrArray
    }

    /**
     * Calculates the averages YCbCr from an RGB block
     * @param pix image pixel two dimensional array
     * @param width image width
     * @param height image height
     * @param iWidth block index in width
     * @param iHeight block index in height
     * @return YCbCr array for the average color of the block
     */
    private fun getBlockColorValue(pix: Array<IntArray>, width: Int, height: Int, iWidth: Int, iHeight: Int): IntArray {
        val wbSize = width / wbNumber
        val hbSize = height / hbNumber
        var totalY = 0.0
        var totalCb = 0.0
        var totalCr = 0.0
        for (inc in iHeight * hbSize until (iHeight + 1) * hbSize) {
            for (i in iWidth * wbSize until (iWidth + 1) * wbSize) {
                val r = Color.red(pix[inc][i]).toDouble()
                val g = Color.green(pix[inc][i]).toDouble()
                val b = Color.blue(pix[inc][i]).toDouble()
                val y = 0.299 * r + 0.587 * g + 0.114 * b
                val cb = 128 - 0.168736 * r - 0.331264 * g + 0.5 * b
                val cr = 128 + 0.5 * r - 0.418688 * g - 0.081312 * b
                totalY += y
                totalCb += cb
                totalCr += cr
            }
        }
        totalY /= (wbSize * hbSize).toDouble()
        totalCb /= (wbSize * hbSize).toDouble()
        totalCr /= (wbSize * hbSize).toDouble()
        return intArrayOf(totalY.toInt(), totalCb.toInt(), totalCr.toInt())
    }

    /**
     * Converts each RGB encoded ints in the array to an RGB array
     * @param array of RGB encoded int
     * @return array of RGB decoded array
     */
    private fun convertYcbcrIntArray(array: IntArray): Array<IntArray> {
        val refArray = Array(array.size) { IntArray(3) }
        for (i in array.indices) {
            refArray[i] = getYCbCr(array[i])
        }
        return refArray
    }

    /**
     * Computes the color (R, G, or B) difference between the screenshot block and the reference block
     * @param colorValue screenshot block color average
     * @param reference reference block color average
     * @return color difference percentage
     */
    private fun getDiffPercentage(colorValue: Int, reference: Int): Double {
        return Math.abs(reference - colorValue) / 255.0 * 100.0
    }

    /**
     * Computes the RGB difference between the screenshot block and the reference block
     * @param colorValue screenshot block RGB array
     * @param reference reference block RGB array
     * @return RGB block average difference
     */
    private fun compareBlockColorValues(colorValue: IntArray, reference: IntArray): Double {
        val y = getDiffPercentage(colorValue[0], reference[0])
        val cb = getDiffPercentage(colorValue[1], reference[1])
        val cr = getDiffPercentage(colorValue[2], reference[2])
        return (y + cb + cr) / 3.0
    }

    /**
     * Compares screenshot blocks and reference blocks and computes the cumulative difference
     * @param colorValues screenshot array of block's RGB arrays
     * @param reference reference array block's RGB arrays
     * @return cumulative color difference between screenshot and reference
     */
    private fun compareImageBlocks(colorValues: Array<IntArray>, reference: Array<IntArray>): Double {
        if (colorValues.size != reference.size) {
            Log.e(TAG, "The color arrays are not of the same size")
            throw RuntimeException()
        }
        var diff = 0.0
        for (i in colorValues.indices) {
            diff += compareBlockColorValues(colorValues[i], reference[i])
        }
        return diff
    }

    /**
     * Checks for presence of vertical transparent ligns on the sides of the screenshot,
     * and removes them if present.
     * Some screenshots can have transparent side if the devices a deactivated notch, or
     * sometimes it can be caused by the navigation bar at the bottom.
     * Failing to remove them at the time the screenshot is taken, they can be removed before
     * analysis.
     * @param filepath screenshot path
     * @param bitmap screenshot bitmap
     * @return a bitmap cropped of it's lignes or the original if their aren't any or if there was
     * a problem.
     */
    private fun cropUselessTransparentSides(filepath: String, bitmap: Bitmap): Bitmap {
        val pix = IntArray(bitmap.width * bitmap.height)
        bitmap.getPixels(pix, 0, bitmap.width, 0, 0, bitmap.width, bitmap.height)
        val pixTab = Array(bitmap.height) { IntArray(bitmap.width) }
        for (i in 0 until bitmap.height) {
            for (inc in 0 until bitmap.width) {
                pixTab[i][inc] = pix[inc + i * bitmap.width]
            }
        }

        /* Determine where the transparent vertical lines are */
        var minWidth = 0
        while (minWidth < bitmap.width && pixTab[0][minWidth] == 0) minWidth++
        var maxWidth = bitmap.width - 1
        while (maxWidth > 0 && pixTab[0][maxWidth] == 0) maxWidth--
        if (minWidth == bitmap.width || minWidth == 0 && maxWidth == bitmap.width - 1) return bitmap

        /* Replacing the old screenshot with the new bitmap*/
        val newBitmap = Bitmap.createBitmap(bitmap, minWidth, 0, maxWidth - minWidth, bitmap.height)
        try {
            val image = File(filepath)
            if (!image.delete()) {
                Log.e(TAG, "cropUselessTransparentSides: Failed to delete old file")
                return bitmap
            }
            val newFile = File(filepath)
            val outputStream = FileOutputStream(newFile)
            newBitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
            outputStream.close()
        } catch (e: IOException) {
            Log.e(TAG, "cropUselessTransparentSides: Failed to save cropped image: $e")
            return bitmap
        }
        bitmap.recycle()
        return newBitmap
    }

    /**
     * Opens screenshot and computes color averages for each blocks
     * @param filepath screenshot filepath
     * @return screenshot's array of block's RGB array
     */
    private fun getColorValues(filepath: String): Array<IntArray> {
        val options = BitmapFactory.Options()
        options.inPreferredConfig = Bitmap.Config.ARGB_8888
        var bitmap = BitmapFactory.decodeFile(filepath, options)
        if (bitmap == null) {
            Log.e(TAG, "Failed to get bitmap from screenshot")
            throw RuntimeException()
        }
        bitmap = cropUselessTransparentSides(filepath, bitmap)
        val pix = IntArray(bitmap.width * bitmap.height)
        bitmap.getPixels(pix, 0, bitmap.width, 0, 0, bitmap.width, bitmap.height)
        val pixTab = Array(bitmap.height) { IntArray(bitmap.width) }
        for (i in 0 until bitmap.height) {
            for (inc in 0 until bitmap.width) {
                pixTab[i][inc] = pix[inc + i * bitmap.width]
            }
        }
        val colorValues = Array(wbNumber * hbNumber) { IntArray(3) }
        for (i in 0 until wbNumber) {
            for (inc in 0 until hbNumber) {
                colorValues[i * hbNumber + inc] = getBlockColorValue(pixTab, bitmap.width, bitmap.height, i, inc)
            }
        }
        bitmap.recycle()
        return colorValues
    }

    /**
     * Computes the percentage of color difference between screenshot and reference
     * and validates screenshot or not
     * @param filepath screenshot filepath
     * @param reference RGB encoded int array of reference from the sample
     * @return color difference percentage between screenshot and reference
     */
    fun validateScreenshot(filepath: String, reference: IntArray): Pair<Boolean, Int> {
        return try {
            val colorValues = getColorValues(filepath)
            val colorReference = convertYcbcrIntArray(reference)
            val diff = compareImageBlocks(colorValues, colorReference)
            Log.i(TAG, "validateScreenshot: screenshots difference percentage: $diff")
            Pair(diff < MAX_SCREENSHOT_COLOR_DIFFERENCE_PERCENT, floor(diff).toInt())
        } catch (e: RuntimeException) {
            Log.e(TAG, "getValidityPercent has failed: ")
            e.printStackTrace()
            Pair(false, -1)
        }
    }
}