/*
 *****************************************************************************
 * OnboardingActivity.kt
 *****************************************************************************
 * Copyright © 2019-2021 VLC authors and VideoLAN
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

package org.videolan.vlcbenchmark.onboarding

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import kotlinx.android.synthetic.main.activity_onboarding.*
import org.videolan.vlcbenchmark.MainPage
import org.videolan.vlcbenchmark.R

class OnboardingActivity : AppCompatActivity(), ViewPager.OnPageChangeListener {

    companion object {
        @Suppress("UNUSED")
        private val TAG = this::class.java.name

        private const val SHARED_PREFERENCE_ONBOARDING = "org.videolan.vlc.gui.video.benchmark.ONBOARDING"
        private const val PERMISSION_REQUEST = 1
    }
    private var permissions: ArrayList<String> = ArrayList()
    private lateinit var positionIndicators: Array<View>
    private lateinit var pager: ViewPager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Checking if the onboarding has already been done
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE)
        val onboardingDone = sharedPref.getBoolean(SHARED_PREFERENCE_ONBOARDING, false)

        // The onboarding process has already been performed -> send user to the main page.
        // Though in case the user has reset his permissions, request permission is called.
        // If the permissions were not requested, requestPermission() will call startMainPage().
        if (onboardingDone) {
            requestPermissions()
        }

        setContentView(R.layout.activity_onboarding)

        positionIndicators = arrayOf(
                onboarding_indicator_0,
                onboarding_indicator_1,
                onboarding_indicator_2
        )

        pager = onboardding_pager
        val layouts = arrayOf(
                R.layout.layout_onboarding_intro,
                R.layout.layout_onboarding_download,
                R.layout.layout_onboarding_warnings
        )
        pager.adapter = OnboardingPagerAdapter(this, layouts)
        pager.currentItem = 0
        updateIndicator(0)
        updateButtons(0)
        pager.addOnPageChangeListener(this)

        // Check if Android TV / chromebook for pad / keyboard inputs -> manually handle focus
        if (packageManager.hasSystemFeature("android.software.leanback")
                or packageManager.hasSystemFeature("org.chromium.arc.device_management")) {
            setFocusListeners()
            onboarding_btn_next.requestFocus()
            // have to call onFocusChange manually here, requestFocus doesn't seem to call the callback
            onFocusChange(onboarding_btn_next, true)
        }
    }

    // In case of android tv, change the background color to clearly indicate focus
    private fun onFocusChange(view: View, hasFocus: Boolean) : Unit {
        if (hasFocus) {
            view.setBackgroundColor(resources.getColor(R.color.grey400transparent))
        } else {
            view.setBackgroundColor(resources.getColor(R.color.white))
        }
    }

    // Setting callbacks to change background on focus change
    private fun setFocusListeners() {
        onboarding_btn_next.setOnFocusChangeListener(::onFocusChange)
        onboarding_btn_previous.setOnFocusChangeListener(::onFocusChange)

        // in case of a tactile swipe, and then an keyboard / pad input, the viewpager
        // gets the focus. To stop that, focus is redirected to next button
        // or done button in the case of the last onboarding view
        pager.setOnFocusChangeListener { _, _ ->
            if (pager.currentItem == 2) {
                onboarding_btn_done.requestFocus()
            } else {
                onboarding_btn_next.requestFocus()
            }
        }
    }

    private fun updateIndicator(position: Int) {
        for (i in positionIndicators.indices) {
            var layoutParams: LinearLayout.LayoutParams
            if (i == position) {
                layoutParams = LinearLayout.LayoutParams(
                        resources.getDimension(R.dimen.selected_indicator).toInt(),
                        resources.getDimension(R.dimen.selected_indicator).toInt())
            } else {
                layoutParams = LinearLayout.LayoutParams(
                        resources.getDimension(R.dimen.unselected_indicator).toInt(),
                        resources.getDimension(R.dimen.unselected_indicator).toInt())
            }
            layoutParams.setMargins(
                    resources.getDimension(R.dimen.indicator_margin).toInt(),
                    resources.getDimension(R.dimen.indicator_margin).toInt(),
                    resources.getDimension(R.dimen.indicator_margin).toInt(),
                    resources.getDimension(R.dimen.indicator_margin).toInt())
            positionIndicators[i].layoutParams = layoutParams
        }
    }

    private fun updateButtons(position: Int) {
        val nextBtn = onboarding_btn_next
        val doneBtn = onboarding_btn_done
        val prevBtn = onboarding_btn_previous
        if (position == 2) {
            nextBtn.visibility = View.INVISIBLE
            doneBtn.visibility = View.VISIBLE
            doneBtn.requestFocus()
        } else {
            nextBtn.visibility = View.VISIBLE
            doneBtn.visibility = View.INVISIBLE
        }
        if (position == 0) {
            prevBtn.visibility = View.INVISIBLE
            nextBtn.requestFocus()
        } else {
            prevBtn.visibility = View.VISIBLE
        }
    }


    fun clickNextPage(view: View) {
        if (pager.currentItem != pager.childCount) {
            pager.currentItem += 1
        }
    }

    fun clickPreviousPage(view: View) {
        if (pager.currentItem != 0) {
            pager.currentItem -= 1
        }
    }

    fun clickDone(view: View) {
        requestPermissions()
    }

    override fun onPageSelected(position: Int) {
        updateIndicator(position)
        updateButtons(position)
    }

    override fun onPageScrollStateChanged(state: Int) {}

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

    private fun requestPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.READ_EXTERNAL_STORAGE)
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }
        if (permissions.size != 0) {
            val arrayPermissions: Array<String> = permissions.toTypedArray()
            ActivityCompat.requestPermissions(this, arrayPermissions, PERMISSION_REQUEST)
        } else {
            startMainPage()
        }
    }

    private fun startMainPage() {
        // Save the fact that the onboarding was done
        val sharedPref = this.getPreferences(Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putBoolean(SHARED_PREFERENCE_ONBOARDING, true)
        editor.apply()

        // Start main page
        val intent = Intent(this, MainPage::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST && grantResults.isNotEmpty()) {
            for (i in grantResults.indices) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                    this.permissions.remove(permissions[i])
                }
            }
            if (this.permissions.isNotEmpty()) {
                AlertDialog.Builder(this)
                        .setTitle(R.string.dialog_title_error)
                        .setMessage(R.string.dialog_text_error_permission)
                        .setNeutralButton(R.string.dialog_btn_ok) { _, _ ->
                            finishAndRemoveTask()
                        }
                        .show()
            } else {
                startMainPage()
            }
        }
    }

    class OnboardingPagerAdapter(context: Context, layouts: Array<Int>) : PagerAdapter() {
        private val mLayouts: Array<Int> = layouts
        private val mContext : Context = context

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val inflater = LayoutInflater.from(mContext)
            val layout = inflater.inflate(mLayouts[position], container, false)
            container.addView(layout)
            return layout
        }

        override fun destroyItem(container: ViewGroup, position: Int, view: Any) {
            container.removeView(view as View)
        }

        override fun isViewFromObject(view: View, viewObject: Any): Boolean {
                return view == viewObject
        }

        override fun getCount(): Int {
            return mLayouts.size
        }
    }
}